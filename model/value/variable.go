package value

type Variable struct {
	Name                 string `json:"name"`
	Value                string `json:"value"`
	BpmnProcessId        string `json:"bpmnProcessId"`
	ProcessDefinitionKey int64  `json:"processDefinitionKey"`
	ProcessInstanceKey   int64  `json:"processInstanceKey"`
	ScopeKey             int64  `json:"scopeKey"`
}
