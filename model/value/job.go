package value

type Job struct {
	Type                     string                 `json:"type"`
	ErrorMessage             string                 `json:"errorMessage"`
	BpmnProcessId            string                 `json:"bpmnProcessId"`
	ProcessDefinitionKey     int64                  `json:"processDefinitionKey"`
	Retries                  int64                  `json:"retries"`
	RetryBackoff             int64                  `json:"retryBackoff"`
	RecurringTime            int64                  `json:"recurringTime"`
	ProcessDefinitionVersion int64                  `json:"processDefinitionVersion"`
	ProcessInstanceKey       int64                  `json:"processInstanceKey"`
	ElementInstanceKey       int64                  `json:"elementInstanceKey"`
	ElementId                string                 `json:"elementId"`
	CustomHeaders            map[string]interface{} `json:"customHeaders"`
	Deadline                 int64                  `json:"deadline"`
	Variables                map[string]interface{} `json:"variables"`
	ErrorCode                string                 `json:"errorCode"`
	Worker                   string                 `json:"worker"`
}
