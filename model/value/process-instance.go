package value

type ProcessInstance struct {
	Version                  int64  `json:"version"`
	BpmnProcessID            string `json:"bpmnProcessId"`
	ProcessDefinitionKey     int64  `json:"processDefinitionKey"`
	PrentElementInstanceKey  int64  `json:"prentElementInstanceKey"`
	ParentProcessInstanceKey int64  `json:"parentProcessInstanceKey"`
	BmnElementType           string `json:"bmnElementType"`
	FlowScopeKey             int64  `json:"flowScopeKey"`
	ProcessInstanceKey       int64  `json:"processInstanceKey"`
	ElementID                string `json:"elementId"`
}
