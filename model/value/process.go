package value

type Process struct {
	BpmnProcessID        string `json:"bpmnProcessID"`
	Checksum             string `json:"checksum"`
	Duplicate            bool   `json:"duplicate"`
	ProcessDefinitionKey int64  `json:"processDefinitionKey"`
	Resource             string `json:"resource"`
	ResourceName         string `json:"resourceName"`
	Version              int64  `json:"version"`
}
