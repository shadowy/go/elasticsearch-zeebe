package model

type Response[T any] struct {
	Hits Hits[T] `json:"hits"`
}

func (r *Response[T]) GetData() []Data[T] {
	return r.Hits.Hits
}
