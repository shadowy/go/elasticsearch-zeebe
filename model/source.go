package model

type Source[T any] struct {
	BrokerVersion        string `json:"brokerVersion"`
	Intent               string `json:"intent"`
	Key                  int64  `json:"key"`
	PartitionID          int64  `json:"partitionId"`
	Position             int64  `json:"position"`
	RecordType           string `json:"recordType"`
	RejectionReason      string `json:"rejectionReason"`
	RejectionType        string `json:"rejectionType"`
	SourceRecordPosition int64  `json:"sourceRecordPosition"`
	Timestamp            int64  `json:"timestamp"`
	Value                T      `json:"value"`
}
