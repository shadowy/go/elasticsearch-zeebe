package model

type Data[T any] struct {
	ID      string    `json:"_id"`
	Index   string    `json:"_index"`
	Routing string    `json:"_routing"`
	Score   float32   `json:"_score"`
	Type    string    `json:"_type"`
	Source  Source[T] `json:"_source"`
}
