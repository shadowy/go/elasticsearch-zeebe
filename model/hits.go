package model

type Hits[T any] struct {
	Hits     []Data[T] `json:"hits"`
	MaxScore float32   `json:"max_score"`
}
