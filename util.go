package lib

import (
	"context"
	"encoding/json"
	"github.com/elastic/go-elasticsearch/v8"
	"gitlab.com/shadowy/go/elasticsearch-zeebe/model"
	"io"
)

func toObject[T any](reader io.Reader) (*T, error) {
	res := new(T)
	if err := json.NewDecoder(reader).Decode(res); err != nil {
		return nil, err
	}
	return res, nil
}

func getCollectionData[T any](client *elasticsearch.Client, collection string, start int, size int) ([]model.Data[T], error) {
	res, err := client.Search(
		client.Search.WithContext(context.Background()),
		client.Search.WithIndex(collection),
		client.Search.WithFrom(start),
		client.Search.WithSize(size),
		client.Search.WithTrackTotalHits(false),
	)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = res.Body.Close()
	}()
	var data *model.Response[T]
	if data, err = toObject[model.Response[T]](res.Body); err != nil {
		return nil, err
	}
	return data.GetData(), nil
}
