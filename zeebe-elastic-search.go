package lib

import (
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/go/elasticsearch-zeebe/model"
	"gitlab.com/shadowy/go/elasticsearch-zeebe/model/value"
)

const (
	collectionProcess         = "zeebe-record-process"
	collectionJob             = "zeebe-record-job"
	collectionProcessInstance = "zeebe-record-process-instance"
	collectionVariable        = "zeebe-record-variable"
)

type ZeebeElasticSearch struct {
	client *elasticsearch.Client

	Servers []string `yaml:"servers"`
}

func (s *ZeebeElasticSearch) Init() error {
	log.Logger.Debug().Msg("ZeebeElasticSearch.Init")
	client, err := elasticsearch.NewClient(elasticsearch.Config{Addresses: s.Servers})
	if err != nil {
		log.Logger.Error().Err(err).Msg("ZeebeElasticSearch.Init connect")
		return err
	}
	s.client = client
	res, err := client.Info()
	if err != nil {
		log.Logger.Error().Err(err).Msg("ZeebeElasticSearch.Init info")
		return err
	}
	defer func() {
		_ = res.Body.Close()
	}()
	info, err := toObject[map[string]interface{}](res.Body)
	if err != nil {
		log.Logger.Error().Err(err).Msg("ZeebeElasticSearch.Init info unmarshal")
		return err
	}
	log.Logger.Debug().Interface("info", info).Msg("ZeebeElasticSearch.Init info")

	client.Watcher.Start()
	return nil
}

func (s *ZeebeElasticSearch) ProcessList(start int, size int) ([]model.Data[value.Process], error) {
	return getCollectionData[value.Process](s.client, collectionProcess, start, size)
}

func (s *ZeebeElasticSearch) JobList(start int, size int) ([]model.Data[value.Job], error) {
	return getCollectionData[value.Job](s.client, collectionJob, start, size)
}

func (s *ZeebeElasticSearch) ProcessInstanceList(start int, size int) ([]model.Data[value.ProcessInstance], error) {
	return getCollectionData[value.ProcessInstance](s.client, collectionProcessInstance, start, size)
}

func (s *ZeebeElasticSearch) VariableList(start int, size int) ([]model.Data[value.Variable], error) {
	return getCollectionData[value.Variable](s.client, collectionVariable, start, size)
}
